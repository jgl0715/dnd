package me.joeyleavell.gdxengine.input;

import com.badlogic.gdx.InputAdapter;

public class AxisSource extends InputAdapter
{

	private InputSource source;
	private int id;

	public AxisSource(InputSource source, int id)
	{
		this.source = source;
		this.id = id;
	}

	public InputSource getSource()
	{
		return source;
	}

	public int getId()
	{
		return id;
	}

}
