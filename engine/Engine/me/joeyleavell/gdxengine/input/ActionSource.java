package me.joeyleavell.gdxengine.input;

import com.badlogic.gdx.Gdx;

public class ActionSource
{

	private InputSource source;
	private int id;

	public ActionSource(InputSource source, int id)
	{
		this.source = source;
		this.id = id;
	}

	public InputSource getSource()
	{
		return source;
	}

	public int getId()
	{
		return id;
	}

	public boolean isActivated()
	{
		switch (source)
		{
		case KEYBOARD:
			return Gdx.input.isKeyPressed(id);
		case MOUSE:
			return Gdx.input.isButtonPressed(id);
		default:
			return false;
		}
	}

}
