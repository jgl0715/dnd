package me.joeyleavell.gdxengine.core;

public abstract class Game
{

	public abstract void create();

	public abstract void update(float delta);

	public abstract void render();

	public abstract void dispose();

}
