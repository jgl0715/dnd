package me.joeyleavell.gdxengine.network;

import java.io.IOException;

import com.badlogic.gdx.utils.DataInput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.event.Event;
import me.joeyleavell.gdxengine.scene.data.DataComponent;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public abstract class RemoteListener implements Runnable
{

	private Thread listeningThread;
	private DataInput input;
	private boolean listening;
	private boolean receivedDisconnect;

	public RemoteListener(DataInput input, String name)
	{
		this.input = input;
		this.listeningThread = new Thread(this, name);
		this.listening = true;
		this.receivedDisconnect = false;
		listeningThread.start();
	}

	public boolean isListening()
	{
		return listening;
	}

	public boolean receivedDisconnect()
	{
		return receivedDisconnect;
	}

	public void stopListening()
	{
		listening = false;

		try
		{
			listeningThread.join();
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	private Object[] deserializeParameters(DataInput input)
	{
		try
		{
			int parameterCount = input.readInt();
			Integer[] parameterIds = new Integer[parameterCount];
			Object[] parameters = new Object[parameterCount];

			for (int i = 0; i < parameterCount; i++)
				parameterIds[i] = input.readInt();
			for (int i = 0; i < parameterCount; i++)
			{
				int parameterId = parameterIds[i];
				parameters[i] = EngineMain.getNetworkManager().deserializeParameter(parameterId, input);
			}

			return parameters;

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void run()
	{
		while (listening)
		{
			try
			{
				// Wait for the next packet
				int packetId = input.readInt();

				// Delegate based on packet type
				switch (packetId)
				{
				case Packets.SPAWN:
					packet_spawn(GameObject.packet_read_spawn(input));
					break;
				case Packets.DISCONNECT:

					receivedDisconnect = true;
					stopListening();

					packet_disconnect(input);
					break;
				case Packets.ID:
					int assignedId = input.readInt();

					packet_id(assignedId);
					break;
				case Packets.REMOVE:
					int idToRemove = input.readInt();
					GameObject objectToRemove = EngineMain.getCurrentWorld().getGameObjectById(idToRemove);

					packet_remove(objectToRemove);

					break;
				case Packets.REPLICATION:

					int objectId = input.readInt();
					int dataId = input.readInt();

					GameObject object = EngineMain.getCurrentWorld().getGameObjectById(objectId);

					DataComponent component = object.findDataComponentById(dataId);
					
					// Fire OnRep event with blank event parameter
					// TODO: does the event parameter need anything special, like the new replicated value?
					component.getOnRepDelegate().fire(new Event());

					packet_replication(component, input);
					break;
				case Packets.RPC:
					String rpcName = input.readUTF();
					Object[] parameters = deserializeParameters(input);
					RemoteProcedure procedure = EngineMain.getNetworkManager().getRemoteProcedure(rpcName);
					procedure.invoke(parameters);

					packet_rpc(rpcName, parameters);
				default:
					// error
					break;
				}
			} catch (IOException e)
			{
				if (!listening)
				{
					System.out.println("Disconnected listener.");
				} else
				{
					e.printStackTrace();
				}
			}
		}
	}

	public abstract void packet_spawn(GameObject spawnedObject) throws IOException;

	public abstract void packet_id(int assignedId) throws IOException;

	public abstract void packet_remove(GameObject objectToRemove) throws IOException;

	public abstract void packet_replication(DataComponent component, DataInput input) throws IOException;

	public abstract void packet_rpc(String remoteProcedure, Object[] parameters) throws IOException;

	public abstract void packet_disconnect(DataInput input) throws IOException;

}
