package me.joeyleavell.gdxengine.scene;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.network.Client;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class GameWorld
{

	public static final float PIXELS_PER_METER = 32.0f;

	private Map<Integer, GameObject> gameObjects;
	private List<GameObject> gameObjectsToAdd;
	private List<GameObject> gameObjectsToRemove;
	private World physicsWorld;

	public GameWorld()
	{
		gameObjects = new ConcurrentHashMap<Integer, GameObject>();
		gameObjectsToAdd = new ArrayList<GameObject>();
		gameObjectsToRemove = new ArrayList<GameObject>();

		physicsWorld = new World(new Vector2(0, 0 / PIXELS_PER_METER), false);
	}

	public World getPhysicsWorld()
	{
		return physicsWorld;
	}

	public void addObject(GameObject object)
	{
		synchronized (gameObjectsToAdd)
		{
			gameObjectsToAdd.add(object);
		}
	}

	public <T extends GameObject> List<T> findObjectsByClass(Class<T> clazz)
	{
		List<T> objects = new ArrayList<T>();
		Iterator<Integer> itr = gameObjects.keySet().iterator();
		Iterator<GameObject> itr2 = gameObjectsToAdd.iterator();

		while (itr.hasNext())
		{
			GameObject next = gameObjects.get(itr.next());
			if (next.getClass() == clazz)
				objects.add(clazz.cast(next));
		}
		while (itr2.hasNext())
		{
			GameObject next = itr2.next();
			if (next.getClass() == clazz)
				objects.add(clazz.cast(next));
		}

		return objects;
	}

	public void spawnAll(DataOutput output)
	{
		synchronized (output)
		{
			Iterator<Integer> objects = gameObjects.keySet().iterator();
			while (objects.hasNext())
			{
				GameObject object = gameObjects.get(objects.next());

				object.packet_send_spawn(output);
			}
		}
	}

	public GameObject getGameObjectById(int objectId)
	{
		synchronized (gameObjectsToAdd)
		{
			GameObject object = gameObjects.get(objectId);
			if (object == null)
			{
				// Perform linear search on objects that are pending add
				for (GameObject pending : gameObjectsToAdd)
					if (pending.getId() == objectId)
						object = pending;
			}

			return object;
		}

	}

	public void addObjectWithReplication(GameObject object)
	{
		addObjectWithReplication(object, null);
	}

	public void addObjectWithReplication(GameObject object, Client spawningClient)
	{
		addObject(object);
		EngineMain.getNetworkManager().spawnOverNetwork(object, spawningClient);
	}

	public void update(float delta)
	{
		synchronized (gameObjectsToAdd)
		{
			int index = 0;
			while (index < gameObjectsToAdd.size())
			{
				GameObject object = gameObjectsToAdd.get(index);

				if (object.hasId())
				{
					// Put the object in the static map and remove the object from the pending list
					gameObjects.put(object.getId(), object);
					gameObjectsToAdd.remove(index);
				} else
				{
					index++;
				}
			}
		}

		synchronized (gameObjectsToRemove)
		{
			Iterator<GameObject> itr = gameObjectsToRemove.iterator();
			while (itr.hasNext())
			{
				GameObject object = itr.next();
				gameObjects.remove(object.getId());

				if (EngineMain.getNetworkManager().isServer())
				{
					EngineMain.getNetworkManager().removeOverNetwork(object);
				}
			}
			gameObjectsToRemove.clear();
		}

		synchronized (gameObjects)
		{
			Iterator<Integer> keysItr = gameObjects.keySet().iterator();
			while (keysItr.hasNext())
			{
				int key = keysItr.next();
				GameObject object = gameObjects.get(key);

				if (object.isDead())
				{
					gameObjectsToRemove.add(object);
				} else
				{
					object.update(delta);
				}
			}
		}

		// Allow the client to run their own physics simulation but usually only the one on the server is authoritative
		physicsWorld.step(delta, 6, 8);
	}

	public void render()
	{
		Iterator<Integer> keysItr = gameObjects.keySet().iterator();

		while (keysItr.hasNext())
		{
			int key = keysItr.next();
			GameObject object = gameObjects.get(key);
			object.render();
		}
	}

}
