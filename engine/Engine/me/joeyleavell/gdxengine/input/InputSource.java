package me.joeyleavell.gdxengine.input;

public enum InputSource
{

	KEYBOARD, MOUSE;

}
