package me.joeyleavell.gdxengine.core;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class EngineLauncher
{

	public static void launchDesktop(Game game, int width, int height)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.foregroundFPS = 60;
		config.width = width;
		config.height = height;
		new LwjglApplication(new EngineMain(game), config);
	}

}
