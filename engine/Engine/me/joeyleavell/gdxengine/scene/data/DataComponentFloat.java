package me.joeyleavell.gdxengine.scene.data;

import java.io.IOException;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.scene.object.GameObject;

public class DataComponentFloat extends DataComponent
{

	public Float value;

	public DataComponentFloat(GameObject parent, String name, boolean serialized, boolean replicated, boolean clientAuthoratative)
	{
		super(parent, name, false, serialized, replicated, clientAuthoratative);

		setCopyCtorParameter(float.class);

		value = 0.0f;
	}

	@Override
	public void send(DataOutput output)
	{
		try
		{
			output.writeFloat(value);
		} catch (IOException e)
		{
			System.err.println("Could not serialize float value " + getName());
			e.printStackTrace();
		}
	}

	@Override
	public void read(DataInput input)
	{
		try
		{
			value = input.readFloat();
		} catch (IOException e)
		{
			System.err.println("Could not serialize float value " + getName());
			e.printStackTrace();
		}
	}

}
