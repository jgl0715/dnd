package me.joeyleavell.gdxengine.scene.data;

import java.io.IOException;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.scene.object.GameObject;

public class DataComponentDouble extends DataComponent
{

	public Double value;

	public DataComponentDouble(GameObject parent, String name, boolean serialized, boolean replicated, boolean clientAuthoratative)
	{
		super(parent, name, false, serialized, replicated, clientAuthoratative);
		
		setCopyCtorParameter(double.class);
		
		value = 0.0;
	}

	@Override
	public void send(DataOutput output)
	{
		try
		{
			output.writeDouble(value);
		} catch (IOException e)
		{
			System.err.println("Could not serialize double value " + getName());
			e.printStackTrace();
		}
	}

	@Override
	public void read(DataInput input)
	{
		try
		{
			value = input.readDouble();
		} catch (IOException e)
		{
			System.err.println("Could not deserialize double value " + getName());
			e.printStackTrace();
		}
	}

}
