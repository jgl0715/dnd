package me.joeyleavell.gdxengine.scene.data;

import java.io.IOException;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.scene.object.GameObject;

public class DataComponentInt extends DataComponent
{
	
	public Integer value;

	public DataComponentInt(GameObject parent, String name, boolean serialized, boolean replicated, boolean clientAuthoratative)
	{
		super(parent, name, false, serialized, replicated, clientAuthoratative);

		setCopyCtorParameter(int.class);

		value = 0;
	}

	@Override
	public void send(DataOutput output)
	{
		try
		{
			output.writeInt(value);
		} catch (IOException e)
		{
			System.err.println("Could not serialize int value " + getName());
			e.printStackTrace();
		}
	}

	@Override
	public void read(DataInput input)
	{
		try
		{
			value = input.readInt();
		} catch (IOException e)
		{
			System.err.println("Could not deserialize int value " + getName());
			e.printStackTrace();
		}
	}

}
