package me.joeyleavell.gdxengine.event;

import java.util.HashMap;
import java.util.Map;

public class Event
{
	
	private Map<String, Object> parameters;
	
	public Event()
	{
		parameters = new HashMap<String, Object>();
	}
	
	public void addParameter(String name, Object parameter)
	{
		parameters.put(name, parameter);
	}
	
	public Object getParameter(String name)
	{
		return parameters.get(name);
	}

}
