package game;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TileManager
{

	private Map<Tile, TextureRegion> tileMappings;
	private Map<Integer, Tile> tileIds;

	public TileManager()
	{
		tileMappings = new HashMap<Tile, TextureRegion>();
		tileIds = new HashMap<Integer, Tile>();
	}

	public void addTile(Tile tile, TextureRegion texture)
	{
		tileMappings.put(tile, texture);
		tileIds.put(tile.getId(), tile);
	}
	
	public Tile getTileFromId(int tile)
	{
		return tileIds.get(tile);
	}
	
	public TextureRegion getTileTexture(int tile)
	{
		return getTileTexture(tileIds.get(tile));
	}

	public TextureRegion getTileTexture(Tile tile)
	{
		return tileMappings.get(tile);
	}

}
