package me.joeyleavell.gdxengine.scene.behavior;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class MovementComponent extends BehaviorComponent
{

	private Vector2f previousPosition;

	public MovementComponent(GameObject parent, String name)
	{
		super(parent, name, true, false);

		addDataComponent(DataComponentVector2f.class, GameObject.POSITION);
		previousPosition = new Vector2f();
	}

	public void update(float delta)
	{
		if (getParent().isOwnedLocally())
		{
			Vector2f position = getDataComponent(DataComponentVector2f.class, GameObject.POSITION).value;
			previousPosition.set(position);

			if (EngineMain.getInputManager().isActionActivated("Up"))
			{
				position.addTo(0, 1);
			}
			if (EngineMain.getInputManager().isActionActivated("Down"))
			{
				position.addTo(0, -1);
			}
			if (EngineMain.getInputManager().isActionActivated("Left"))
			{
				position.addTo(-1, 0);
			}
			if (EngineMain.getInputManager().isActionActivated("Right"))
			{
				position.addTo(1, 0);
			}

			// Check if the component has been moved
//			if (!position.equals(previousPosition))
//				getDataComponent(DataComponentVector2f.class, GameObject.POSITION).changed();
		}

	}

}
