package me.joeyleavell.gdxengine.event;

import me.joeyleavell.gdxengine.network.Client;

public class ClientJoinedEvent extends Event
{

	public ClientJoinedEvent(Client client)
	{
		addParameter("Client", client);
	}

}
