package me.joeyleavell.gdxengine.scene.behavior;

import java.util.HashMap;
import java.util.Map;

import me.joeyleavell.gdxengine.scene.data.DataComponent;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public abstract class BehaviorComponent
{

	private GameObject parent;
	private String name;
	private Map<String, DataComponent> mappedComponents;
	protected boolean updates;
	protected boolean renders;

	public BehaviorComponent(GameObject parent, String name, boolean updates, boolean renders)
	{
		this.parent = parent;
		this.name = name;
		this.mappedComponents = new HashMap<String, DataComponent>();
		this.updates = updates;
		this.renders = renders;
	}

	public void addDataComponent(Class<? extends DataComponent> clazz, String name)
	{
		// O(N) search through parent game object
		DataComponent component = parent.findDataComponentByClassAndName(clazz, name);

		if (component == null)
			throw new IllegalArgumentException("Could not find data component with the name " + name);
		else
			mappedComponents.put(name, component);
	}

	public <T extends DataComponent> T getDataComponent(Class<T> clazz, String name)
	{
		// O(1) retrieval through hash map
		DataComponent component = mappedComponents.get(name);

		if (component == null)
			throw new IllegalArgumentException("Data component with the name " + name + " not registered");
		else
			return clazz.cast(component);
	}

	public GameObject getParent()
	{
		return parent;
	}

	public String getName()
	{
		return name;
	}

	public boolean doesUpdate()
	{
		return updates;
	}

	public boolean doesRender()
	{
		return renders;
	}

	public void init()
	{

	}

	public void update(float delta)
	{

	}

	public void render()
	{

	}

}
