package game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.network.RPCType;
import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.data.DataComponentBody;
import me.joeyleavell.gdxengine.scene.object.DynamicBox;

public class Player extends DynamicBox
{

	private Body body;

	public Player(GameWorld gameWorld)
	{
		super(gameWorld, 10, 100, 0, 0);

		body = findDataComponentByClassAndName(DataComponentBody.class, BOUNDS).value;

		EngineMain.getNetworkManager().makeRpc(this, "RPC_ApplyForce", RPCType.SERVER);
	}

	public Player(GameWorld gameWorld, float x, float y)
	{
		super(gameWorld, 10, 100, x, y);

		body = findDataComponentByClassAndName(DataComponentBody.class, BOUNDS).value;

		EngineMain.getNetworkManager().makeRpc(this, "RPC_ApplyForce", RPCType.SERVER);
	}

	public void RPC_ApplyForce(Float xAmount, Float yAmount)
	{
		body.applyForceToCenter(new Vector2(xAmount, yAmount), true);
		
		System.out.println("test " + xAmount + " " + yAmount);
	}

	public boolean RPC_ApplyForce_Validate(Float xAmount, Float yAmount)
	{
		return true;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if (isAuthoritative())
		{

			if (isOwnedLocally())
			{
				if (EngineMain.getInputManager().isActionActivated("Up"))
				{
					body.applyForceToCenter(new Vector2(0, 4), true);
				}
				if (EngineMain.getInputManager().isActionActivated("Down"))
				{
					body.applyForceToCenter(new Vector2(0, -4), true);
				}
			}

			/*
			 * if (EngineMain.getInputManager().isActionActivated("Shoot") &&
			 * shootTimer.can()) { Vector2f bulletPos = new Vector2f(position.x +
			 * box.getWidth() / 2, position.y + box.getHeight() / 2); float mouseAngle =
			 * EngineMain.getInputManager().getMouseAngle((int) bulletPos.x, (int)
			 * bulletPos.y); float velX = (float) (Math.cos(mouseAngle) * 5); float velY =
			 * (float) (Math.sin(mouseAngle) * 5); world.addObjectWithReplication(new
			 * Bullet(world, EngineMain.getNetworkManager().isServer(), bulletPos, new
			 * Vector2f(velX, velY))); }
			 */
		} else
		{
			float yAmount = 0.0f;

			if (EngineMain.getInputManager().isActionActivated("Up"))
				yAmount = 4.0f;
			if (EngineMain.getInputManager().isActionActivated("Down"))
				yAmount = -4.0f;

			if (yAmount != 0)
			{
				EngineMain.getNetworkManager().networkRpc("RPC_ApplyForce", 0.0f, yAmount);
			}
		}
	}

}
