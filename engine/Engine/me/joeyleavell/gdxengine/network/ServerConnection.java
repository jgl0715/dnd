package me.joeyleavell.gdxengine.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.scene.object.GameObject;

public class ServerConnection
{

	private Socket serverConnection;
	private DataInput socketInput;
	private DataOutput socketOutput;
	private ServerRemoteListener listener;
	private boolean connected;

	/**
	 * 
	 * @param serverAddress
	 * @param serverPort
	 */
	public ServerConnection(String serverAddress, int serverPort)
	{
		try
		{
			serverConnection = new Socket(InetAddress.getByName(serverAddress), serverPort);
			socketInput = new DataInput(serverConnection.getInputStream());
			socketOutput = new DataOutput(serverConnection.getOutputStream());
			listener = new ServerRemoteListener(socketInput);
			connected = true;
			System.out.println("Successfully connected to server.");
		} catch (IOException e)
		{
			System.err.println("Could not connect to server!");
			e.printStackTrace();
			connected = false;
		}
	}

	public boolean isConnected()
	{
		return connected;
	}

	public DataOutput getSocketOutput()
	{
		return socketOutput;
	}

	public void spawnObjectOnServer(GameObject object)
	{

		synchronized (socketOutput)
		{
			// Put the object on a queue to get its ID
			listener.addObjectAwaitingId(object);

			System.out.println("spawning to server");
			object.packet_send_spawn(socketOutput);
		}

	}

	public void disconnectFromServer()
	{
		try
		{
			synchronized (socketOutput)
			{
				if (connected)
				{
					connected = false;

					// Send disconnect packet
					try
					{
						socketOutput.writeInt(Packets.DISCONNECT);
					} catch (IOException e1)
					{
						e1.printStackTrace();
					}

					serverConnection.close();

					listener.stopListening();
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
