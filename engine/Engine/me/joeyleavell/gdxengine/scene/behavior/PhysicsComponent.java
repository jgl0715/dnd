package me.joeyleavell.gdxengine.scene.behavior;

import com.badlogic.gdx.physics.box2d.Transform;

import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.data.DataComponentBody;
import me.joeyleavell.gdxengine.scene.data.DataComponentFloat;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class PhysicsComponent extends BehaviorComponent
{

	public PhysicsComponent(GameObject parent, String name)
	{
		super(parent, name, true, false);

		addDataComponent(DataComponentVector2f.class, GameObject.POSITION);
		addDataComponent(DataComponentFloat.class, GameObject.ROTATION);
		addDataComponent(DataComponentBody.class, GameObject.BOUNDS);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		// We only care about updating the position of the object if this is the server
		if (getParent().isAuthoritative())
		{
			Vector2f position = getDataComponent(DataComponentVector2f.class, GameObject.POSITION).value;
			DataComponentFloat rotation = getDataComponent(DataComponentFloat.class, GameObject.ROTATION);
			Transform transform = getDataComponent(DataComponentBody.class, GameObject.BOUNDS).value.getTransform();

			// Copy the physics transform into the components
			// TODO: make own transform class?
			position.set(transform.getPosition().x * GameWorld.PIXELS_PER_METER, transform.getPosition().y * GameWorld.PIXELS_PER_METER);
			rotation.value = transform.getRotation();
		}
	}

}
