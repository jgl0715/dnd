package me.joeyleavell.gdxengine.event;

public interface EventListener
{

	public void fire(Event event);

}
