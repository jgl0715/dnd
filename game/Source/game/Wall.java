package game;

import com.badlogic.gdx.Gdx;

import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.object.StaticBox;

public class Wall extends StaticBox
{

	public Wall(GameWorld gameWorld)
	{
		super(gameWorld, Gdx.graphics.getWidth() - 1, 10, 0, 0);
	}

	public Wall(GameWorld gameWorld, float x, float y)
	{
		super(gameWorld, Gdx.graphics.getWidth() - 1, 10, x, y);
	}

}
