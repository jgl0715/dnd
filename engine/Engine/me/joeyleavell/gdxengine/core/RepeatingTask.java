package me.joeyleavell.gdxengine.core;

import com.badlogic.gdx.utils.Timer.Task;

public abstract class RepeatingTask extends Task
{

	private float intervalSeconds;

	public RepeatingTask(float intervalSeconds)
	{
		this.intervalSeconds = intervalSeconds;
	}

	public float getIntervalSeconds()
	{
		return intervalSeconds;
	}

}
