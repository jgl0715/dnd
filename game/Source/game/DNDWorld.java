package game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.scene.GameWorld;

public class DNDWorld extends GameWorld
{

	public static final int TILE_SIZE = 16;

	private int width;
	private int height;
	private Tile[] tiles;

	public DNDWorld(int width, int height)
	{
		this.width = width;
		this.height = height;
		tiles = new Tile[width * height];
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Tile getTile(int row, int col)
	{
		return tiles[row * width + col];
	}

	public void setTile(Tile value, int col, int row)
	{
		tiles[row * width + col] = value;
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
	}

	@Override
	public void render()
	{
		super.render();

		SpriteBatch sr = EngineMain.getSpriteBatch();
		for (int row = 0; row < height; row++)
		{
			for (int column = 0; column < width; column++)
			{
				Tile tile = tiles[row * width + column];
				if (tile != null)
				{
					TextureRegion tileTexture = MainGame.getTileManager().getTileTexture(tile.getId());

					sr.begin();
					if (tileTexture != null)
					{
						sr.draw(tileTexture, column * TILE_SIZE, row * TILE_SIZE);
					}
					sr.end();
				}
			}
		}
	}

}
