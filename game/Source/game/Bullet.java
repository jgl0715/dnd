package game;

import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.behavior.VelocityComponent;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;
import me.joeyleavell.gdxengine.scene.object.GOBox;

public class Bullet extends GOBox
{

	public Bullet(GameWorld world, boolean server)
	{
		super(world, server, 5, 5);

		addDataComponent(new DataComponentVector2f(this, VELOCITY, true, true, false));
		addBehaviorComponent(new VelocityComponent(this, VELOCITY, true, false));
	}

	public Bullet(GameWorld world, boolean server, Vector2f pos, Vector2f vel)
	{
		super(world, server, 5, 5);

		DataComponentVector2f velocityComponent = new DataComponentVector2f(this, VELOCITY, true, true, false);
		velocityComponent.value.set(vel);

		addDataComponent(velocityComponent);
		addBehaviorComponent(new VelocityComponent(this, VELOCITY, true, false));
		findDataComponentByClassAndName(DataComponentVector2f.class, POSITION).value.set(pos);
	}

}
