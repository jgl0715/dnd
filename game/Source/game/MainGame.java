package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import game.states.EditorState;
import me.joeyleavell.gdxengine.core.EngineLauncher;
import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.core.Game;
import me.joeyleavell.gdxengine.input.ActionSource;
import me.joeyleavell.gdxengine.input.AxisSource;
import me.joeyleavell.gdxengine.input.InputSource;
import me.joeyleavell.gdxengine.input.Mouse;

public class MainGame extends Game
{

	public static final int WIDTH = 16 * 60;
	public static final int HEIGHT = 9 * 60;

	public static final int VIEWPORT_WIDTH = 16 * 30;
	public static final int VIEWPORT_HEIGHT = 9 * 30;

	// States
	public static final String LEVEL_EDITOR_STATE = "Level Editor";

	// Input
	public static final String INPUT_UP = "Up";
	public static final String INPUT_DOWN = "Down";
	public static final String INPUT_LEFT = "Left";
	public static final String INPUT_RIGHT = "Right";
	public static final String INPUT_PLACE_TILE = "Place_Tile";
	public static final String INPUT_DELETE_TILE = "Delete_Tile";

	public static final String INPUT_CHANGE_TILE = "Change_Tile";

	private static TileManager tileManager;

	public MainGame()
	{

	}

	public static TileManager getTileManager()
	{
		return tileManager;
	}

	@Override
	public void create()
	{
		EngineMain.getStateManager().registerState(LEVEL_EDITOR_STATE, new EditorState(), true);

		EngineMain.getInputManager().registerAction(INPUT_UP);
		EngineMain.getInputManager().registerAction(INPUT_DOWN);
		EngineMain.getInputManager().registerAction(INPUT_LEFT);
		EngineMain.getInputManager().registerAction(INPUT_RIGHT);
		EngineMain.getInputManager().registerAction(INPUT_PLACE_TILE);
		EngineMain.getInputManager().registerAction(INPUT_DELETE_TILE);

		EngineMain.getInputManager().registerAxis(INPUT_CHANGE_TILE);

		EngineMain.getInputManager().registerActionBinding(INPUT_UP, new ActionSource(InputSource.KEYBOARD, Keys.W));
		EngineMain.getInputManager().registerActionBinding(INPUT_DOWN, new ActionSource(InputSource.KEYBOARD, Keys.S));
		EngineMain.getInputManager().registerActionBinding(INPUT_LEFT, new ActionSource(InputSource.KEYBOARD, Keys.A));
		EngineMain.getInputManager().registerActionBinding(INPUT_RIGHT, new ActionSource(InputSource.KEYBOARD, Keys.D));
		EngineMain.getInputManager().registerActionBinding(INPUT_PLACE_TILE, new ActionSource(InputSource.MOUSE, Buttons.LEFT));
		EngineMain.getInputManager().registerActionBinding(INPUT_DELETE_TILE, new ActionSource(InputSource.MOUSE, Buttons.RIGHT));
		EngineMain.getInputManager().registerAxisBinding(INPUT_CHANGE_TILE, new AxisSource(InputSource.MOUSE, Mouse.AXIS_SCROLL));

		Texture tilesTexture = new Texture(Gdx.files.internal("Textures/Tiles.png"));

		tileManager = new TileManager();

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				int index = j * 5 + i;
				tileManager.addTile(new Tile(index), new TextureRegion(tilesTexture, (DNDWorld.TILE_SIZE + 1) * i, (DNDWorld.TILE_SIZE + 1) * j, DNDWorld.TILE_SIZE, DNDWorld.TILE_SIZE));
			}
		}

	}

	@Override
	public void update(float delta)
	{

	}

	@Override
	public void render()
	{

	}

	@Override
	public void dispose()
	{

	}

	public static void main(String[] args)
	{
		EngineLauncher.launchDesktop(new MainGame(), WIDTH, HEIGHT);
	}

}
