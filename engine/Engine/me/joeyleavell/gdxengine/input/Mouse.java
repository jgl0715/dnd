package me.joeyleavell.gdxengine.input;

public class Mouse
{
	
	public static final int AXIS_X      = 0;
	public static final int AXIS_Y      = 1;
	public static final int AXIS_SCROLL = 2;

}
