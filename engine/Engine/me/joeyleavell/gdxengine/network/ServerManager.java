package me.joeyleavell.gdxengine.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.event.ClientJoinedEvent;
import me.joeyleavell.gdxengine.event.MulticastDelegate;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class ServerManager
{

	// TODO: use thread pool?

	private ServerSocket serverSocket;
	private List<Client> clients;
	private List<Client> clientsToRemove;
	private ByteArrayOutputStream multicastBuffer;
	private DataOutput multicastOutput;
	private Thread connectionListener;
	private boolean running;
	private MulticastDelegate clientJoinedDelegate;

	public ServerManager()
	{
		clients = new ArrayList<Client>();
		clientsToRemove = new ArrayList<Client>();
		multicastBuffer = new ByteArrayOutputStream();
		multicastOutput = new DataOutput(multicastBuffer);

		clientJoinedDelegate = new MulticastDelegate();
	}

	public MulticastDelegate getClientJoinedDelegate()
	{
		return clientJoinedDelegate;
	}
	
	public void startServer()
	{
		startServer(0);
	}

	public void startServer(int port)
	{
		running = true;
		try
		{
			serverSocket = new ServerSocket(port);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		connectionListener = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (running)
				{
					try
					{
						Socket socket = serverSocket.accept();
						Client newClient = new Client(socket, socket.getInetAddress(), socket.getPort());

						// Replicate all current game objects to newly connected
						// client
						EngineMain.getCurrentWorld().spawnAll(newClient.getSocketOutput());

						clients.add(newClient);

						// Fire event
						clientJoinedDelegate.fire(new ClientJoinedEvent(newClient));
					} catch (IOException e)
					{
						// If the server isn't running anymore, the SocketClosed
						// exception is expected
						if (running == false)
						{
							System.out.println("Server stopped.");
						} else
						{
							System.err.println("Server encountered an unexpected error.");
							e.printStackTrace();
						}
					}
				}
			}
		});
		connectionListener.start();
	}

	public void stop()
	{
		try
		{
			running = false;

			// Multicast disconnect packet
			multicastOutput.writeInt(Packets.DISCONNECT);
			multicast();

			// Stop server socket
			serverSocket.close();

			// Disconnect all clients from server socket
			for (Client client : clients)
				client.disconnect();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

		try
		{
			connectionListener.join();
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public DataOutput getMulticastOutput()
	{
		return multicastOutput;
	}

	public synchronized void multicast()
	{
		multicast(null);
	}

	public synchronized void clientSpawn(GameObject object, Client spawnTo)
	{
		object.packet_send_spawn(spawnTo.getSocketOutput());
	}

	public synchronized void multicastSpawn(GameObject object, Client spawningClient)
	{
		object.packet_send_spawn(multicastOutput);
		multicast(spawningClient);
	}

	public void multicast(Client avoid)
	{
		synchronized (multicastOutput)
		{
			byte[] data = multicastBuffer.toByteArray();

			try
			{
				// Multicast to all clients but the specified avoid client
				for (Client client : clients)
				{
					if (!client.isConnected())
						clientsToRemove.add(client);
					else if (client != avoid && client.isConnected())
						client.getSocketOutput().write(data, 0, multicastBuffer.size());
				}

				clients.removeAll(clientsToRemove);
				clientsToRemove.clear();

			} catch (IOException e)
			{
				e.printStackTrace();
			}

			// Reset the buffer output
			multicastBuffer.reset();
		}
	}

	public boolean isRunning()
	{
		return running;
	}

}
