package me.joeyleavell.gdxengine.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.util.vector.Vector2f;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class InputManager implements InputProcessor
{

	private List<String> actions;
	private List<String> axes;
	private Map<String, List<ActionSource>> actionBinds;
	private Map<String, List<AxisSource>> axisBinds;

	private int scrollAmount;

	public InputManager()
	{
		actions = new ArrayList<String>();
		axes = new ArrayList<String>();
		actionBinds = new HashMap<String, List<ActionSource>>();
		axisBinds = new HashMap<String, List<AxisSource>>();
	}

	public void registerAction(String actionName)
	{
		if (actions.contains(actionName))
			throw new IllegalStateException("Action " + actionName + " already registered!");
		actions.add(actionName);
		actionBinds.put(actionName, new ArrayList<ActionSource>());
	}

	public void registerAxis(String axisName)
	{
		if (axes.contains(axisName))
			throw new IllegalStateException("Axis " + axisName + " already registered!");
		axes.add(axisName);
		axisBinds.put(axisName, new ArrayList<AxisSource>());
	}

	public void registerActionBinding(String action, ActionSource input)
	{
		// TODO: make custom exception handling and logging system
		if (!actions.contains(action))
			throw new IllegalStateException("Action " + action + " not registered!");
		else
			actionBinds.get(action).add(input);
	}

	public void registerAxisBinding(String axis, AxisSource input)
	{
		// TODO: make custom exception handling and logging system
		if (!axes.contains(axis))
			throw new IllegalStateException("Axis " + axis + " not registered!");
		else
			axisBinds.get(axis).add(input);
	}

	public boolean isActionActivated(String actionName)
	{
		List<ActionSource> actionSources = actionBinds.get(actionName);

		// Check all input sources registered with this action.
		for (ActionSource source : actionSources)
			if (source.isActivated())
				return true;
		return false;
	}

	public float getAxisValue(String axisName)
	{
		List<AxisSource> axisSources = axisBinds.get(axisName);

		// Check all input sources registered with this action.
		for (AxisSource source : axisSources)
		{
			switch (source.getSource())
			{
			case MOUSE:

				if (source.getId() == Mouse.AXIS_X)
					return Gdx.input.getX();
				else if (source.getId() == Mouse.AXIS_Y)
					return Gdx.input.getY();
				else if (source.getId() == Mouse.AXIS_SCROLL)
					return scrollAmount;

				break;
			default:
				break;
			}
		}
		return 0;
	}

	public Vector2f getVectorToMouse(int screenX, int screenY)
	{
		return new Vector2f(Gdx.input.getX() - screenX, (Gdx.graphics.getHeight() - Gdx.input.getY()) - screenY);
	}

	public float getMouseAngle(int screenX, int screenY)
	{
		Vector2f mouseVec = getVectorToMouse(screenX, screenY);

		return (float) Math.atan2(mouseVec.y, mouseVec.x);
	}

	public void update(float delta)
	{
		// reset scroll amount
		this.scrollAmount = 0;
	}

	@Override
	public boolean keyDown(int keycode)
	{

		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		this.scrollAmount = amount;

		return true;
	}

}
