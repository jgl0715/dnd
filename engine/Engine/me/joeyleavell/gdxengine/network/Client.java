package me.joeyleavell.gdxengine.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

public class Client
{

	private Socket socket;
	private InetAddress address;
	private int port;
	private ClientRemoteListener listener;
	private DataInput socketIn;
	private DataOutput socketOut;

	public Client(Socket socket, InetAddress address, int port)
	{
		this.socket = socket;
		this.address = address;
		this.port = port;
		try
		{
			this.socketIn = new DataInput(socket.getInputStream());
			this.socketOut = new DataOutput(socket.getOutputStream());
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		listener = new ClientRemoteListener(this);
	}

	public boolean isConnected()
	{
		return listener.isListening();
	}

	public void disconnect()
	{

		try
		{
			// Officially close the client socket
			socket.close();

			// Join the thread
			listener.stopListening();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public Socket getSocket()
	{
		return socket;
	}

	public DataInput getSocketInput()
	{
		return socketIn;
	}

	public DataOutput getSocketOutput()
	{
		return socketOut;
	}

	public InetAddress getAddress()
	{
		return address;
	}

	public int getPort()
	{
		return port;
	}

}
