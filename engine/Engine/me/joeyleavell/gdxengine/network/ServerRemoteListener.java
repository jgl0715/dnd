package me.joeyleavell.gdxengine.network;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import com.badlogic.gdx.utils.DataInput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.scene.data.DataComponent;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class ServerRemoteListener extends RemoteListener
{

	private Queue<GameObject> idQueue;

	public ServerRemoteListener(DataInput input)
	{
		super(input, "Server-Listener");

		this.idQueue = new LinkedList<GameObject>();
	}

	public void addObjectAwaitingId(GameObject object)
	{
		synchronized (idQueue)
		{
			idQueue.offer(object);
		}
	}

	@Override
	public void packet_spawn(GameObject spawnedObject) throws IOException
	{
		EngineMain.getCurrentWorld().addObject(spawnedObject);
	}

	@Override
	public void packet_id(int assignedId) throws IOException
	{
		synchronized (idQueue)
		{
			GameObject queuedObject = idQueue.poll();
			queuedObject.setId(assignedId);
		}
	}

	@Override
	public void packet_remove(GameObject objectToRemove) throws IOException
	{
		objectToRemove.remove();
	}

	@Override
	public void packet_replication(DataComponent component, DataInput input) throws IOException
	{
		component.read(input);
	}

	@Override
	public void packet_disconnect(DataInput input) throws IOException
	{

	}

	@Override
	public void packet_rpc(String remoteProcedure, Object[] parameters) throws IOException
	{
		
	}

}
