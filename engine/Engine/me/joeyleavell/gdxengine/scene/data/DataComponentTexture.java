package me.joeyleavell.gdxengine.scene.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import me.joeyleavell.gdxengine.scene.object.GameObject;

public class DataComponentTexture extends DataComponent
{
	
	public Texture value;

	public DataComponentTexture(GameObject parent, String name, String path)
	{
		super(parent, name, false, false, false, true);
		
		value = new Texture(Gdx.files.internal(path));
	}

}
