package game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.DNDWorld;
import game.MainGame;
import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.state.GameState;

public class EditorState extends GameState {

	private OrthographicCamera camera;
	private Viewport viewport;
	private DNDWorld world;

	private Vector2f min;
	private Vector2f max;

	private int tileId;

	@Override
	public void create() {
		camera = new OrthographicCamera(MainGame.VIEWPORT_WIDTH, MainGame.VIEWPORT_HEIGHT);
		viewport = new StretchViewport(MainGame.VIEWPORT_WIDTH, MainGame.VIEWPORT_HEIGHT, camera);
		world = new DNDWorld(32, 32);
		EngineMain.setCurrentWorld(world);

		min = new Vector2f(getHalfScreenWidth(), getHalfScreenHeight());
		max = new Vector2f(world.getWidth() * DNDWorld.TILE_SIZE - getHalfScreenWidth(),
				world.getHeight() * DNDWorld.TILE_SIZE - getHalfScreenHeight());
	}

	@Override
	public void show() {
		camera.position.set(min.x, min.y, 0);
		camera.update();
	}

	public int getHalfScreenWidth() {
		return (int) camera.viewportWidth / 2;
	}

	public int getHalfScreenHeight() {
		return (int) camera.viewportHeight / 2;
	}

	@Override
	public void hide() {

	}

	public int getMouseWorldX(OrthographicCamera camera) {
		return (int) camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).x;
	}

	public int getMouseWorldY(OrthographicCamera camera) {
		return (int) camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).y;
	}

	@Override
	public void update() {
		int mouseTileX = getMouseWorldX(camera) / DNDWorld.TILE_SIZE;
		int mouseTileY = getMouseWorldY(camera) / DNDWorld.TILE_SIZE;

		if (EngineMain.getInputManager().isActionActivated(MainGame.INPUT_UP) && camera.position.y < max.y) {
			camera.translate(0, 1);
		}
		if (EngineMain.getInputManager().isActionActivated(MainGame.INPUT_DOWN) && camera.position.y >= min.y) {
			camera.translate(0, -1);
		}
		if (EngineMain.getInputManager().isActionActivated(MainGame.INPUT_LEFT) && camera.position.x >= min.x) {
			camera.translate(-1, 0);
		}
		if (EngineMain.getInputManager().isActionActivated(MainGame.INPUT_RIGHT) && camera.position.x < max.x) {
			camera.translate(1, 0);
		}

		tileId += EngineMain.getInputManager().getAxisValue(MainGame.INPUT_CHANGE_TILE);
		if(tileId < 0)
			tileId = 0;
		if(tileId >= 15)
			tileId = 14;

		if (EngineMain.getInputManager().isActionActivated(MainGame.INPUT_PLACE_TILE)) {
			world.setTile(MainGame.getTileManager().getTileFromId(tileId), mouseTileX, mouseTileY);
		}
		if (EngineMain.getInputManager().isActionActivated(MainGame.INPUT_DELETE_TILE)) {
			world.setTile(null, mouseTileX, mouseTileY);
		}

		camera.update();
	}

	@Override
	public void render() {

		ShapeRenderer sr = EngineMain.getShapeRenderer();
		SpriteBatch sb = EngineMain.getSpriteBatch();

		EngineMain.getShapeRenderer().setTransformMatrix(camera.view);
		EngineMain.getShapeRenderer().setProjectionMatrix(camera.projection);

		EngineMain.getSpriteBatch().setTransformMatrix(camera.view);
		EngineMain.getSpriteBatch().setProjectionMatrix(camera.projection);

		int mouseTileX = getMouseWorldX(camera) / DNDWorld.TILE_SIZE;
		int mouseTileY = getMouseWorldY(camera) / DNDWorld.TILE_SIZE;
		
		sb.begin();
		sb.draw(MainGame.getTileManager().getTileTexture(tileId), 10, 10);
		sb.end();

		sr.begin(ShapeType.Line);
		sr.setColor(Color.WHITE);

		for (int row = 0; row <= world.getWidth(); row++) {
			sr.line(0, row * DNDWorld.TILE_SIZE, world.getWidth() * DNDWorld.TILE_SIZE, row * DNDWorld.TILE_SIZE);
		}
		for (int column = 0; column <= world.getHeight(); column++) {
			sr.line(column * DNDWorld.TILE_SIZE, 0, column * DNDWorld.TILE_SIZE,
					world.getHeight() * DNDWorld.TILE_SIZE);
		}

		if (mouseTileX >= 0 && mouseTileX < world.getWidth() && mouseTileY >= 0 && mouseTileY < world.getHeight()) {
			sr.setColor(Color.GREEN);
			sr.rect(mouseTileX * DNDWorld.TILE_SIZE, mouseTileY * DNDWorld.TILE_SIZE, DNDWorld.TILE_SIZE,
					DNDWorld.TILE_SIZE);
		}

		sr.end();
	}

	@Override
	public void dispose() {

	}

}
