package game;

public class Tile
{
	
	private int id;
	
	public Tile(int id)
	{
		this.id = id;
	}
	
	public int getId()
	{
		return id;
	}

}
