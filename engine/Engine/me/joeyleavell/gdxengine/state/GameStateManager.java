package me.joeyleavell.gdxengine.state;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

public class GameStateManager
{

	private Map<String, GameState> stateMap;
	private Stack<GameState> states;

	public GameStateManager()
	{
		stateMap = new HashMap<String, GameState>();
		states = new Stack<GameState>();
	}

	public void registerState(String stateName, GameState state, boolean push)
	{
		stateMap.put(stateName, state);
		state.create();

		if (push)
		{
			pushState(stateName);
		}
	}

	public boolean isValidState(String stateName)
	{
		if (stateMap.containsKey(stateName))
		{
			return true;
		} else
		{
			return false;
		}
	}

	public GameState getCurrentState()
	{
		if (states.isEmpty())
			return null;
		else
			return states.peek();
	}

	public void pushState(String stateName)
	{
		if (isValidState(stateName))
		{
			GameState current = getCurrentState();
			GameState newState = stateMap.get(stateName);
			if (current != null)
			{
				current.setSecondary(true);
				current.hide();
			}

			states.push(newState);
			newState.show();
		} else
		{
			throw new IllegalArgumentException(stateName + " is not a valid state");
		}
	}

	public void popState()
	{
		GameState current = getCurrentState();
		if (current != null)
		{
			states.pop();
			current.hide();
			current = states.peek();

			if (current != null)
			{
				current.show();
				current.setSecondary(false);
			}
		}
	}

	public void update()
	{
		getCurrentState().update();
	}

	public void render()
	{
		getCurrentState().render();
	}

	public void dispose()
	{
		Iterator<String> keyItr = stateMap.keySet().iterator();
		while (keyItr.hasNext())
		{
			String key = keyItr.next();
			stateMap.get(key).dispose();
		}
	}

}
