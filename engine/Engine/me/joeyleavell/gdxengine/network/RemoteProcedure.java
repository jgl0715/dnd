package me.joeyleavell.gdxengine.network;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RemoteProcedure
{

	public static final int PARAMETER_INT = 0x00;
	public static final int PARAMETER_FLOAT = 0x01;
	public static final int PARAMETER_DOUBLE = 0x02;
	public static final int PARAMETER_STRING = 0x03;
	public static final int PARAMETER_BOOLEAN = 0x04;

	private Object object;
	private String name;
	private RPCType type;
	private Method implementation;
	private Method validation;
	private Class<?>[] parameterTypes;
	private int parameterCount;

	public RemoteProcedure(Object object, String rpcName, RPCType type)
	{
		this.object = object;
		this.name = rpcName;
		this.type = type;

		// Search for method
		Method[] methods = object.getClass().getDeclaredMethods();
		for (Method method : methods)
		{
			if (method.getName().equals(rpcName))
				this.implementation = method;
			else if (method.getName().equals(rpcName + "_Validate"))
				this.validation = method;
		}
		

		// Check if the method implementation was found
		if (implementation == null)
			throw new IllegalArgumentException("Could not find method by the name of " + rpcName);

		// Check if the method implementation return type is void
		if (implementation.getReturnType() != Void.TYPE)
			throw new IllegalArgumentException("Implementation method's return type must be void");

		this.parameterCount = implementation.getParameterCount();
		this.parameterTypes = implementation.getParameterTypes();

		// Check if we're multicast and we have a validate method
		if (type == RPCType.MULTICAST && validation != null)
			throw new IllegalArgumentException("Multicast remote procedures cannot have validate methods");

		if (type == RPCType.SERVER && validation == null)
			throw new IllegalArgumentException("Server remote procedures require validate methods");

		// Make sure the validate method is declared correctly (must return boolean and
		// match parameter signature of the implementation method)
		if (validation != null)
		{
			Class<?>[] methodParameters = implementation.getParameterTypes();
			Class<?>[] validateParameters = validation.getParameterTypes();
			
			System.out.println(methodParameters);

			if (validation.getReturnType() != Boolean.TYPE)
				throw new IllegalArgumentException("Validate method must return a boolean");

			boolean matching = true;
			if (methodParameters.length == validateParameters.length)
			{
				for (int i = 0; i < parameterCount; i++)
				{
					if (!methodParameters[i].equals(validateParameters[i]))
						matching = false;
				}
			} else
			{
				matching = false;
			}

			if (!matching)
				throw new IllegalArgumentException("Validate method signature must match implementation signature");

		}

	}

	public boolean checkParameters(Object[] parameters)
	{
		boolean correct = true;

		if (parameterTypes.length == parameters.length)
		{
			for (int i = 0; i < parameterCount; i++)
			{
				if (parameters[i].getClass() != parameterTypes[i])
				{
					System.out.println(parameters[i].getClass() + " " + parameterTypes[i]);
					correct = false;
				}
			}
		} else
		{
			correct = false;
		}

		return correct;
	}

	public void invoke(Object[] parameters)
	{
		try
		{
			boolean canExecute = true;
			if (hasValidation())
				canExecute = (Boolean) validation.invoke(object, parameters);

			if (canExecute)
			{
				implementation.invoke(object, parameters);
			} else
			{
				System.out.println("Validation failed!");
			}
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		} catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
	}

	public String getName()
	{
		return name;
	}

	public boolean hasValidation()
	{
		return validation != null;
	}

	public Method getImplementation()
	{
		return implementation;
	}

	public Method getValidation()
	{
		return validation;
	}

	public RPCType getType()
	{
		return type;
	}

	public int getParameterCount()
	{
		return parameterCount;
	}

}
