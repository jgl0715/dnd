package me.joeyleavell.gdxengine.state;

public abstract class GameState
{

	private boolean secondary;

	public GameState()
	{
		this.secondary = false;
	}

	public boolean isSecondary()
	{
		return secondary;
	}

	public void setSecondary(boolean secondary)
	{
		this.secondary = secondary;
	}

	public abstract void create();

	public abstract void show();

	public abstract void hide();

	public abstract void update();

	public abstract void render();

	public abstract void dispose();

}
